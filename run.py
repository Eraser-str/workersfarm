import restapi.views
import admin.views
import social_auth.routes
from config import app


if __name__ == "__main__":
    app.run()