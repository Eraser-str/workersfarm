import pymongo


def db_connect():
    conn = pymongo.Connection()
    db = conn['workers']
    return db

db = db_connect()