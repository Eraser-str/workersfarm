from celery import Celery

celery = Celery('tasks', broker='amqp://', backend='amqp')

WORKER_SLEEP = 1
WORKER_INCREMENT = 1