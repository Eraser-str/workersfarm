import time

from models import db
from workers.config_local import celery, WORKER_INCREMENT, WORKER_SLEEP
from workers.utils import generate_exit_status


@celery.task
def worker(sleep_time, owner, task_id):
    sec = 0
    # init task in database
    db.tasks.insert({
        "_id": task_id,
        "owner": owner,
        "sleep_time": sleep_time,
        "time_in_sleep": sec,
        "status": "in_progress"
    })

    # main loop of task
    while sec < sleep_time:
        time.sleep(WORKER_SLEEP)
        sec += WORKER_INCREMENT
        db.tasks.update(
            {"_id": task_id},
            {'$inc': {"time_in_sleep": WORKER_INCREMENT}}
        )

    # record the state of the task at the time exit of the cycle
    exit_status = generate_exit_status()
    if exit_status > 0:
        db.tasks.update(
                {"_id": task_id},
                {'$set': {"status": "error"}}
            )
    else:
        db.tasks.update(
                {"_id": task_id},
                {'$set': {"status": "finished_ok"}}
            )
    return exit_status