class Responses:
    def success(self, resp):
        return {
            "response": resp
        }

    def bad_request(self, err_val):
        return {
            "response": {
                "error": err_val
            }
        }

Responses = Responses()