# -*- coding: utf-8 -*-
#TODO: limit and offset в выдачах

from flask import request, jsonify

from config import app
from models import db
from utils import generate_random_id
from restapi.config_local import API_PREFIX
from restapi.decorators import api_authenticated, required_args
from restapi.response_patterns import Responses
from workers.tasks import worker


@app.route('{0}task/start'.format(API_PREFIX), methods=['GET', 'POST'])
@api_authenticated
@required_args(['sleep_time'])
def start_task():
    """Метод, запускающий воркер.

    :param api_key: ключ, позволяющий работать с апи
    :param sleep_time: время, которое воркер должен работать (спать)

    :type api_key: str
    :type sleep_time: int

    :return: статус и идентификатор воркера
    :rtype: json

    """
    try:
        sleep_time = int(request.args.get('sleep_time'))
    except ValueError:
        return jsonify(Responses.bad_request('\'sleep_time\' argument must be int'))
    api_key = request.args.get('api_key')
    task_id = generate_random_id()

    worker.delay(sleep_time, api_key, task_id)

    return jsonify(Responses.success({"_id": task_id, "status": "in_progress"})), 201


@app.route('{0}task/<task_id>/status'.format(API_PREFIX), methods=['GET'])
@api_authenticated
def status_task(task_id):
    """Возвращает статус воркера по его идентификатору. Можно передавать несколько id, разделяя их запятой.

    :param api_key: ключ, позволяющий работать с апи
    :param task_id: id воркера

    :type api_key: str
    :type task_id: строка идентификаторов, разделенных запятыми

    :return: массив воркеров с полями _id, sleep_time, status, time_in_sleep
    :rtype: json array

    """
    api_key = request.args.get('api_key')
    response = []

    for task in set(task_id.split(',')):
        result = db.tasks.find_one({"owner": api_key, "_id": task}, {'owner': 0})
        if result:
            response.append(result)

    return jsonify({'response': response})


@app.route('{0}task/filter'.format(API_PREFIX), methods=['GET'])
@api_authenticated
@required_args(['status'])
def get_tasks():
    """Возвращает список тасков для текущего юзера, со статусами, соответствующими переданым в параметре status

    :param api_key: ключ, позволяющий работать с апи
    :param status: статус воркера (in_progress, finished_ok, error)

    :type api_key: str
    :type task_id: строка статусов, разделенных запятыми

    :return: массив воркеров с полями _id, sleep_time, status, time_in_sleep
    :rtype: json array

    """
    api_key = request.args.get('api_key')
    status_list = set(request.args.get('status').split(','))
    response = []

    for status in status_list:
        result = list(db.tasks.find({"owner": api_key, "status": status}, {'owner': 0}))
        if result:
            response.extend(result)

    return jsonify(Responses.success(response))