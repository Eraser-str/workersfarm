# -*- coding: utf-8 -*-
from functools import wraps

from flask import request, jsonify

from restapi.response_patterns import Responses


def api_authenticated(fun):
    """Декоратор проверяет наличие аргумента api_key в запросе и пользователя с таким api_key в базе данных. Если
    такого аргумента или пользователя нет, то пользователю возвращается ошибка авторизации.

    """
    @wraps(fun)
    def wrapper(*args, **kwargs):
        if not request.args.get('api_key'):
            return jsonify(Responses.bad_request('api_key is required argument and must be submitted)')), 401
        return fun(*args, **kwargs)
    return wrapper


def required_args(arg_list):
    """Декторатор используется для проверки наличия обязательных параметров (аргументов) и возвращает ошибку в случае
    отсутствия хотя бы одного из них

    """
    def decorator(fun):
        @wraps(fun)
        def wrapper(*args, **kwargs):
            for a in arg_list:
                if not request.args.get(a):
                    return jsonify(Responses.bad_request('argument \'{0}\' is required and must be submitted'.format(a)))
            return fun(*args, **kwargs)
        return wrapper
    return decorator