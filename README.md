## Сервер

    python run.py

## Доступные методы апи

http://127.0.0.1:5000/**api/v1/task/start**?api_key=<key>&sleep_time=<time>

http://127.0.0.1:5000/**api/v1/task/**<task_id>**/status**?api_key=<key>

http://127.0.0.1:5000/**api/v1/task/filter**?api_key=<key>&status=<status>

## Тест

    python benchmark.py [-h] -n N -t T
