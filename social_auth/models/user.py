from flask.ext.login import UserMixin

from social_auth import db
from utils import generate_random_id


class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(200))
    password = db.Column(db.String(200), default='')
    name = db.Column(db.String(100))
    email = db.Column(db.String(200))
    active = db.Column(db.Boolean, default=True)
    api_key = db.Column(db.String(40), default=generate_random_id())

    def is_active(self):
        return self.active
