SECRET_KEY = 'random-secret-key'
SESSION_COOKIE_NAME = 'psa_session'

from os.path import dirname, abspath
SQLALCHEMY_DATABASE_URI = 'sqlite:////%s/test.db' % dirname(abspath(__file__))

DEBUG_TB_INTERCEPT_REDIRECTS = False
SESSION_PROTECTION = 'strong'

SOCIAL_AUTH_LOGIN_URL = '/'
SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/dashboard/'
SOCIAL_AUTH_USER_MODEL = 'social_auth.models.user.User'
SOCIAL_AUTH_AUTHENTICATION_BACKENDS = (
    'social.backends.vk.VKOAuth2',
    # 'social.backends.facebook.FacebookOAuth2'
)
