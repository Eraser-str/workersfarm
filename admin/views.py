from flask import render_template, redirect, url_for
from flask.ext.login import login_required, logout_user

from config import app


@app.route('/')
def main():
    return render_template('home.html')


@app.route('/dashboard/')
@login_required
def done():
    return render_template('dashboard.html')


@app.route('/logout/')
def logout():
    logout_user()
    return redirect('/')