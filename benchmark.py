# -*- coding: utf-8 -*-
import time
import argparse

import pymongo
import requests

# db
conn = pymongo.Connection()
db = conn['workersfarmbenchmark']

# create parser and add arguments
parser = argparse.ArgumentParser(description='Нагрузочный тест')
parser.add_argument('-n', type=int, help='Количество воркеров', required=True)
parser.add_argument('-t', type=int, help='Время работы воркеров', required=True)
# parse args
args = vars(parser.parse_args())
workers_num = args['n']
sleep_time = args['t']

# vars for requests
BASE_URL = 'http://127.0.0.1:5000/'
API_KEY = '111111'
tasks_list = []

# create workers
for i in range(workers_num):
    r = requests.get('{host}api/v1/{method}?{args}'.format(
        host=BASE_URL,
        method='task/start',
        args='api_key={0}&sleep_time={1}'.format(API_KEY, sleep_time)
    ))
    tasks_list.append(r.json()['response']['_id'])

# callback with user
pause_time = workers_num * sleep_time + 3
print 'Подождите {0} секунд(ы). Идет работа воркеров.'.format(pause_time)
time.sleep(pause_time)

# get status of workers
r = requests.get('{host}api/v1/{method}?{args}'.format(
    host=BASE_URL,
    method='task/' + ','.join(tasks_list) + '/status',
    args='api_key=' + API_KEY
))

status_list = [worker['status'] for worker in r.json()['response']]
finished_ok = status_list.count('finished_ok')
error = status_list.count('error')

# insert data to db
for worker in r.json()['response']:
    db.benchmark.insert(worker)

print db.benchmark.count()
print db.benchmark.find({'status': 'error'}).count()

print 'finished_ok:', finished_ok
print 'error:', error
print 'процент ошибок в этом тесте: ' + str(float(100 / (finished_ok + error) * error)) + '%'
print 'процент ошибок всего: ' + str(
    float(100 / db.benchmark.count() * db.benchmark.find({'status': 'error'}).count())) + '%'